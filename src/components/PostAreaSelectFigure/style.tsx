import styled from 'styled-components'

export const PostAreaSelectFigure = styled.div`
  width: 88px;
  height: 88px;
  border-radius: 38px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border: 2px solid var(--black-three);
  position: relative;
`
