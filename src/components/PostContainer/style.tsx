import styled from 'styled-components'

export const PostContainer = styled.section`
  width: 100%;
  height: auto;
  padding: 24px;
  margin-bottom: 16px;
  border-radius: 3px;
  background-color: var(--black-two);
  border: solid 1px #3b3b3b;
`
