import styled from 'styled-components'

export const Container = styled.main`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 40px 20px 0 20px;
`
export const ContainerPost = styled.div`
  width: 100%;
  max-width: 516px;
`
