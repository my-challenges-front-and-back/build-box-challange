import styled from 'styled-components'

export const FeedArea = styled.div`
  width: 100%;
  margin-top: 56px;
  margin-bottom: 56px;
`

export const FeedTitleArea = styled.div`
  width: 100%;
  margin-bottom: 8px;
`

export const FeedTitle = styled.span`
  color: #7a7a7a;
  font-size: 14px;
  font-weight: 500;
`

export const FeedPost = styled.div`
  width: 100%;
`
