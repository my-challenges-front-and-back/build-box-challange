import styled from 'styled-components'

export const Input = styled.input`
  width: 100%;
  border: none;
  height: 40px;
  font-size: 14px;
  padding: 16px 12px;
  border-radius: 8px;
  outline: 0;
  background: #494949;
  color: #fff;

  &::placeholder {
    color: #9f9f9f;
  }
`
