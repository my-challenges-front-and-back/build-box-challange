import styled from 'styled-components'

export const Header = styled.header`
  width: 100%;
  height: 93px;
  background: #2b2b2b;
  display: flex;
  justify-content: center;
  align-items: center;
`
export const HeaderLogo = styled.img`
  width: 100%;
  max-width: 143px;
  height: 45px;
`
