import React from 'react'

/* stlye */
import * as S from './style'

const Header = () => {
  return (
    <S.Header>
      <S.HeaderLogo src="assets/images/bx-logo.svg" />
    </S.Header>
  )
}

export default Header
